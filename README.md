# Unified hosts file with base extensions

#### AsusWRT and Asuswrt-Merlin
```bash
# Use the below command to get the hosts
curl -o /etc/hosts https://gitlab.com/hunter/hosts/raw/main/hosts
cp -rf /etc/hosts /jffs/configs/hosts

# Flush DNS cache
killall -SIGHUP dnsmasq
```

#### Mac OS
```bash
# Use the below command to get the hosts
sudo curl -o /etc/hosts https://gitlab.com/hunter/hosts/raw/main/hosts

# Flush DNS cache
sudo killall -HUP mDNSResponder
```

#### Unix and Linux
```bash
# Use the below command to get the hosts
sudo curl -o /etc/hosts https://gitlab.com/hunter/hosts/raw/main/hosts

# Flush DNS cache
sudo systemctl restart dnsmasq
sudo systemctl restart nscd
```

#### Windows
```
Windows PowerShell (Admin)
```
```powershell
# Use the below command to get the hosts
[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
Invoke-WebRequest -OutFile "C:\Windows\System32\drivers\etc\hosts" -Uri "https://gitlab.com/hunter/hosts/raw/main/hosts"

# Flush DNS cache
ipconfig /flushdns
```

